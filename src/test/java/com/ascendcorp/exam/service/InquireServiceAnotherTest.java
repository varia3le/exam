package com.ascendcorp.exam.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.Silent.class)

public class InquireServiceAnotherTest {
    @InjectMocks
    InquiryService testService;

    @Test
    public void testValidateValueAndThrowIfNullCaseValueIsNull() {
        try {
            testService.validateValueAndThrowIfNull(null, "errorMessage");
        } catch (Exception e) {
            assertEquals("errorMessage", e.getMessage());
        }
    }

    @Test
    public void testValidateValueAndThrowIfNullCaseValueIsEmpty() {
        try {
            testService.validateValueAndThrowIfNull("", "errorMessage");
        } catch (Exception e) {
            assertEquals("errorMessage", e.getMessage());
        }
    }

    @Test
    public void testValidateValueAndThrowIfNullCaseValueIsNotEmpty() {
        boolean actual = testService.validateValueAndThrowIfNull("test", "errorMessage");
        assertTrue(actual);
    }

    @Test
    public void testValidateValueAndThrowIfNullCaseValueIsDate() {
        boolean actual = testService.validateValueAndThrowIfNull(new Date(), "errorMessage");
        assertTrue(actual);
    }

    @Test
    public void testValidateValueAndThrowIfLessthanZeroCaseMoreZero() {
        boolean actual = testService.validateValueAndThrowIfLessthanZero(1D, "errorMessage");
        assertTrue(actual);
    }

    @Test
    public void testValidateValueAndThrowIfLessthanZeroCaseLessthanZero() {
        try {
            testService.validateValueAndThrowIfLessthanZero(-1D, "errorMessage");
        } catch (Exception e) {
            assertEquals("errorMessage", e.getMessage());
        }
    }

    @Test
    public void testValidateValueAndThrowIfLessthanZeroCaseEqualZero() {
        try {
            testService.validateValueAndThrowIfLessthanZero(0D, "errorMessage");
        } catch (Exception e) {
            assertEquals("errorMessage", e.getMessage());
        }
    }

}
