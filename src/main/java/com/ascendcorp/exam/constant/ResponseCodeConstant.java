package com.ascendcorp.exam.constant;

public class ResponseCodeConstant {
    public static final String APPROVED = "approved";
    public static final String INVALID_DATA = "invalid_data";
    public static final String TRANSACTION_ERROR = "transaction_error";
    public static final String UNKNOW = "unknown";
    public static final String CODE_98 = "98";
}
