package com.ascendcorp.exam.model;

import java.util.Date;

import lombok.Data;

@Data
public class InquiryRequest {
    private String transactionId;
    private Date tranDateTime;
    private String channel;
    private String locationCode;
    private String bankCode;
    private String bankNumber;
    private double amount;
    private String reference1;
    private String reference2;
    private String firstName;
    private String lastName;
}
