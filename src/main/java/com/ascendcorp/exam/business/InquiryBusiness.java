package com.ascendcorp.exam.business;

import org.springframework.stereotype.Component;

@Component
public class InquiryBusiness {

    public <T> boolean validateValueAndThrowIfNull(T value, String errorMessage) {
        if (value == null) {
            throw new NullPointerException(errorMessage);
        }
        return true;
    }

}
