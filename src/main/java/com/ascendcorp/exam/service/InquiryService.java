package com.ascendcorp.exam.service;

import com.ascendcorp.exam.constant.ResponseCodeConstant;
import com.ascendcorp.exam.model.InquiryRequest;
import com.ascendcorp.exam.model.InquiryServiceResultDTO;
import com.ascendcorp.exam.model.TransferResponse;
import com.ascendcorp.exam.proxy.BankProxyGateway;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import jakarta.xml.ws.WebServiceException;

public class InquiryService {
    private @Autowired BankProxyGateway bankProxyGateway;
    final static Logger log = Logger.getLogger(InquiryService.class);

    public InquiryServiceResultDTO inquiry(InquiryRequest request) {
        InquiryServiceResultDTO responseDTO = null;
        try {
            validateRequestInquiry(request);
            TransferResponse tranferResponse = bankProxyGateway.requestTransfer(request.getTransactionId(),
                    request.getTranDateTime(), request.getChannel(), request.getBankCode(), request.getBankNumber(),
                    request.getAmount(), request.getReference1(), request.getReference2());
            if (tranferResponse != null) {
                responseDTO = setValueIfFoundResponseCode(tranferResponse);
                switch (tranferResponse.getResponseCode().toLowerCase()) {
                    case (ResponseCodeConstant.APPROVED):
                        convertCaseApproved(responseDTO, tranferResponse);
                        break;
                    case (ResponseCodeConstant.INVALID_DATA):
                        convertCaseInvalidData(responseDTO, tranferResponse);
                        break;
                    case (ResponseCodeConstant.TRANSACTION_ERROR):
                        convertCaseTransactionError(responseDTO, tranferResponse);
                        break;
                    case (ResponseCodeConstant.UNKNOW):
                        convertCaseUnknow(responseDTO, tranferResponse);
                        break;
                    default:
                        throw new Exception("Unsupport Error Reason Code");
                }
            } else {
                throw new Exception("Unable to inquiry from service.");
            }
        } catch (NullPointerException ne) {
            if (responseDTO == null) {
                responseDTO = throwExceptionError("500", "General Invalid Data");
            }
        } catch (WebServiceException r) {
            String faultString = r.getMessage();
            responseDTO = throwWevServiceTimeout(responseDTO, faultString);
        } catch (Exception e) {
            responseDTO = (responseDTO == null || (responseDTO != null && responseDTO.getReasonCode() == null))
                    ? throwExceptionError("504", "Internal Application Error")
                    : responseDTO;
        }
        return responseDTO;
    }

    public <T> boolean validateValueAndThrowIfNull(T value, String errorMessage) {
        if (value == null || value == ("")) {
            throw new NullPointerException(errorMessage);
        }
        return true;
    }

    public boolean validateValueAndThrowIfLessthanZero(double value, String errorMessage) {
        if (value <= 0) {
            throw new NullPointerException(errorMessage);
        }
        return true;
    }

    InquiryServiceResultDTO setValueIfFoundResponseCode(TransferResponse response) {
        InquiryServiceResultDTO responseDTO = new InquiryServiceResultDTO();
        responseDTO.setRef_no1(response.getReferenceCode1());
        responseDTO.setRef_no2(response.getReferenceCode2());
        responseDTO.setAmount(response.getAmount());
        responseDTO.setTranID(response.getBankTransactionID());
        return responseDTO;
    }

    private void validateRequestInquiry(InquiryRequest request) {
        validateValueAndThrowIfNull(request.getTransactionId(), "Transaction id is required!");
        validateValueAndThrowIfNull(request.getTranDateTime(), "Transaction DateTime is required!");
        validateValueAndThrowIfNull(request.getChannel(), "Channel is required!");
        validateValueAndThrowIfNull(request.getBankCode(), "Bank Code is required!");
        validateValueAndThrowIfNull(request.getBankNumber(), "Bank Number is required!");
        validateValueAndThrowIfLessthanZero(request.getAmount(), "Amount must more than zero!");
    }

    public InquiryServiceResultDTO convertCaseApproved(InquiryServiceResultDTO responseDTO,
            TransferResponse tranferResponse) {
        responseDTO.setReasonCode("200");
        responseDTO.setReasonDesc(tranferResponse.getDescription());
        responseDTO.setAccountName(tranferResponse.getDescription());
        return responseDTO;
    }

    public InquiryServiceResultDTO convertCaseInvalidData(InquiryServiceResultDTO responseDTO,
            TransferResponse tranferResponse) {
        String replyDesc = tranferResponse.getDescription();
        responseDTO.setReasonCode("400");
        responseDTO.setReasonDesc("General Invalid Data");
        if (replyDesc != null) {
            String respDesc[] = replyDesc.split(":");
            if (respDesc != null && respDesc.length >= 3) {
                responseDTO.setReasonCode(respDesc[1]);
                responseDTO.setReasonDesc(respDesc[2]);
            }
        }
        return responseDTO;
    }

    public InquiryServiceResultDTO convertCaseTransactionError(InquiryServiceResultDTO responseDTO,
            TransferResponse tranferResponse) {
        String replyDesc = tranferResponse.getDescription();
        responseDTO.setReasonCode("500");
        responseDTO.setReasonDesc("General Transaction Error");
        if (replyDesc != null) {
            String respDesc[] = replyDesc.split(":");
            if (respDesc != null && respDesc.length >= 2) {
                String subIdx1 = respDesc[0];
                String subIdx2 = respDesc[1];
                responseDTO.setReasonCode(subIdx1);
                responseDTO.setReasonDesc(subIdx2);
                if (!subIdx1.equalsIgnoreCase(ResponseCodeConstant.CODE_98) && respDesc.length >= 3) {
                    String subIdx3 = respDesc[2];
                    responseDTO.setReasonCode(subIdx2);
                    responseDTO.setReasonDesc(subIdx3);
                }
            }
        }
        return responseDTO;
    }

    public InquiryServiceResultDTO convertCaseUnknow(InquiryServiceResultDTO responseDTO,
            TransferResponse tranferResponse) {
        String replyDesc = tranferResponse.getDescription();
        responseDTO.setReasonCode("501");
        responseDTO.setReasonDesc("General Invalid Data");
        if (replyDesc != null) {
            String respDesc[] = replyDesc.split(":");
            if (respDesc != null && respDesc.length >= 2) {
                responseDTO.setReasonCode(respDesc[0]);
                responseDTO.setReasonDesc(respDesc[1]);
                if (responseDTO.getReasonDesc() == null || responseDTO.getReasonDesc().trim().length() == 0) {
                    responseDTO.setReasonDesc("General Invalid Data");
                }
            }
        }
        return responseDTO;
    }

    private InquiryServiceResultDTO throwWevServiceTimeout(InquiryServiceResultDTO responseDTO, String faultString) {
        if (responseDTO == null) {
            responseDTO = throwExceptionError("504", "Internal Application Error");
            if (faultString != null && (faultString.indexOf("java.net.SocketTimeoutException") > -1
                    || faultString.indexOf("Connection timed out") > -1)) {
                responseDTO = throwExceptionError("503", "Error timeout");
            }
        }
        return responseDTO;
    }

    private InquiryServiceResultDTO throwExceptionError(String reasonCode, String reasonDesc) {
        InquiryServiceResultDTO error = new InquiryServiceResultDTO();
        error.setReasonCode(reasonCode);
        error.setReasonDesc(reasonDesc);
        return error;
    }
}
